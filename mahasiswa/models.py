from __future__ import unicode_literals
from django.db import models

#import modul user yanng digunakan untuk mengelola user login
from django.contrib.auth.models import User

#model Mahasiswa digunakan untuk membuat table mahasiswa
class Mahasiswa(models.Model):
    JK_CHOICES = (('1','Pria'),('w','Wanita'))
    PS_CHOICES = (('mi','Manajemen Informatika'),('ak','Akuntansi'),('me','Mesin'))

    #definisi field dalam tabel Mahasiswa
    nama = models.CharField('Nama Mahasiswa',max_length=50)
    alamat = models.CharField(max_length=100)
    jk = models.CharField('Kelamin',max_length=1,choices=JK_CHOICES)
    ps = models.CharField('Program Studi',max_length=2,choices=PS_CHOICES)
    nim = models.CharField(max_length=30)
    no_telp = models.CharField('No.HP',max_length=30,blank=True)
    email = models.CharField('E-mail',max_length=100,blank=True)

    def __unicode__(self):
        return self.nama

